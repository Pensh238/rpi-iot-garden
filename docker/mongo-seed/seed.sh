#!/bin/sh
mongoimport --host mongodb --db sensors-data --collection air-humidity --type json --file /air-humidity-seed.json --jsonArray;
mongoimport --host mongodb --db sensors-data --collection air-temperature --type json --file /air-temperature-seed.json --jsonArray;
mongoimport --host mongodb --db sensors-data --collection soil-humidity --type json --file /soil-humidity-seed.json --jsonArray