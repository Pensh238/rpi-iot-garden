# Description
Raspberry Pi IoT garden project using Python, Spring Boot and Flutter

# Softwares

## Sensors
Python interfacing program for reading sensor data and saving it to database via Restful API
*For more information on sensors data models, check out Mongo seed json files*

## API
Spring Boot RESTful API for CRUD operations on sensor data via MongoDB server

## Frontend Application
Flutter app to display and 

# Material used
* Raspberry Pi 4 Model B (https://www.raspberrypi.org/products/raspberry-pi-4-model-b/)
* DHT11 humidity and temperature sensor (https://components101.com/dht11-temperature-sensor)
* Capacitive soil moisture sensor v1.2 (https://how2electronics.com/interface-capacitive-soil-moisture-sensor-arduino/)

### Docker MongoDB Usage

## Related Documentation

* [Mongodb Data import documentation](https://docs.mongodb.com/getting-started/shell/import-data/)
* [How do I seed a mongodb database using docker-compose](https://stackoverflow.com/questions/31210973/how-do-i-seed-a-mongo-database-using-docker-compose)

## How to

### Seeding database for the first time

```bash
docker-compose up -d
docker-compose -f docker-compose.seed.yml up -d
```

or

```bash
docker-compose -f docker-compose.yml -f docker-compose.seed.yml up -d
```

### When already seeded

As usual, just run

```bash
docker-compose up -d
```
