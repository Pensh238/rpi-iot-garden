import 'dart:convert';
import 'dart:developer';
import 'dart:ui';

import 'package:bson/bson.dart';
import 'package:flutter/material.dart';
import 'package:iot_garden_flutter/model/AirHumidity.dart';
import 'package:iot_garden_flutter/service/NetworkCall.dart';
import 'package:iot_garden_flutter/viewmodel/SensorDataVisualizerViewModel.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class AirHumidityVizualizerViewModel
    implements SensorDataVisualizerViewModel<AirHumidity> {
  @override
  String chartTitle;

  @override
  List<AirHumidity> dataSet;

  @override
  double interval;

  @override
  double maxValue;

  @override
  double minValue;

  @override
  String xAxisTitle;

  @override
  String yAxisTitle;

  AirHumidityVizualizerViewModel(this.chartTitle, this.xAxisTitle,
      this.yAxisTitle, this.minValue, this.maxValue,this.interval) {
    getDataSetFromApi();
  }

  @override
  List<LineSeries<AirHumidity, DateTime>> getStaticLineSeries() {
    return <LineSeries<AirHumidity, DateTime>>[
      LineSeries<AirHumidity, DateTime>(
        dataSource: dataSet,
        xValueMapper: (AirHumidity data, _) =>
            ObjectId.fromHexString(data.id).dateTime,
        yValueMapper: (AirHumidity data, _) => data.value,
        color: Color.fromRGBO(143, 188, 187, 1),
      )
    ];
  }

  void getDataSetFromApi() {
    NetworkCall networkCall = NetworkCall();
    var jsonResponse =
        networkCall.getAllPaginated("air-humidity", 0, 10).toString();
    var list = json.decode(jsonResponse);
    for (var data in list) {
      dataSet.add(AirHumidity.fromJson(data));
    }
  }
}
