import 'package:json_annotation/json_annotation.dart';
import 'package:iot_garden_flutter/model/ISensorData.dart';

part 'AirTemperature.g.dart';

@JsonSerializable()
class AirTemperature implements ISensorData {
  @override
  @JsonKey(name: '_id', required: true)
  String id;
  @override
  double value;

  AirTemperature(this.id, this.value);

  factory AirTemperature.fromJson(Map<String, dynamic> json) =>
      _$AirTemperatureFromJson(json);

  Map<String, dynamic> toJson() => _$AirTemperatureToJson(this);
}
