import 'package:json_annotation/json_annotation.dart';
import 'package:iot_garden_flutter/model/ISensorData.dart';

part 'SoilMoisture.g.dart';

@JsonSerializable()
class SoilMoisture implements ISensorData {
  @override
  @JsonKey(name: '_id', required: true)
  String id;
  @override
  double value;

  SoilMoisture(this.id, this.value);

  factory SoilMoisture.fromJson(Map<String, dynamic> json) =>
      _$SoilMoistureFromJson(json);

  Map<String, dynamic> toJson() => _$SoilMoistureToJson(this);
}
