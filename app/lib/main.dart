import 'package:flutter/material.dart';
import 'package:iot_garden_flutter/view/AirHumidityChartView.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Raspberry Pi Garden",
      home: Scaffold(
        appBar: AppBar(title: Text("Raspberry Pi Garden")),
        body: AirHumidityChartView(),
      ),
    );
  }
}

