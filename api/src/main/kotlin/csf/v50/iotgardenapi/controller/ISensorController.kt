package csf.v50.iotgardenapi.controller

import org.springframework.data.domain.Page
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

interface ISensorController<ISensorData> {

    @GetMapping("/")
    fun getAllPaginated(
            @RequestParam("page") page: Int,
            @RequestParam("size") size: Int
    ): ResponseEntity<Page<ISensorData>>

    @GetMapping("/{id}")
    fun getById(@PathVariable id: String): ResponseEntity<ISensorData>

    @PostMapping("/add")
    fun save(@RequestBody obj: ISensorData): ResponseEntity<ISensorData>
}