package csf.v50.iotgardenapi.controller

import csf.v50.iotgardenapi.model.AirHumidity
import csf.v50.iotgardenapi.services.AirHumidityService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("air-humidity")
class AirHumidityController: ISensorController<AirHumidity> {

    @Autowired
    private lateinit var service: AirHumidityService

    override fun getAllPaginated(page: Int, size: Int): ResponseEntity<Page<AirHumidity>> {
        val task: Page<AirHumidity>

        try {
            task = service.GetAllPaginated(PageRequest.of(page, size))
            return ResponseEntity(task, HttpStatus.CREATED)
        } catch (e: Exception) {
            return ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    override fun getById(id: String): ResponseEntity<AirHumidity> {
        val task: AirHumidity

        try {
            task = service.getById(id)
            return ResponseEntity(task, HttpStatus.CREATED)
        } catch (e: Exception) {
            return ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    override fun save(obj: AirHumidity): ResponseEntity<AirHumidity> {
        val task: AirHumidity

        try {
            task = service.create(obj)
            return ResponseEntity(task, HttpStatus.CREATED)
        } catch (e: Exception) {
            return ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
}