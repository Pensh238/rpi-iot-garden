package csf.v50.iotgardenapi.controller

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping
class DefaultController {
    @GetMapping
    fun getDefautltMessage(): ResponseEntity<String> {
        val message = "Bienvenue dans l'API Spring Boot pour jardin connecté Raspberry Pi!"
        return ResponseEntity(message, HttpStatus.ACCEPTED)
    }
}