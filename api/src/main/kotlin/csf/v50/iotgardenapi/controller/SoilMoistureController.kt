package csf.v50.iotgardenapi.controller

import csf.v50.iotgardenapi.model.SoilMoisture
import csf.v50.iotgardenapi.services.SoilMoistureService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("soil-moisture")
class SoilMoistureController : ISensorController<SoilMoisture> {

    @Autowired
    private lateinit var service: SoilMoistureService

    override fun getAllPaginated(page: Int, size: Int): ResponseEntity<Page<SoilMoisture>> {
        val task: Page<SoilMoisture>

        try {
            task = service.GetAllPaginated(PageRequest.of(page, size))
            return ResponseEntity(task, HttpStatus.CREATED)
        } catch (e: Exception) {
            return ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    override fun getById(id: String): ResponseEntity<SoilMoisture> {
        val task: SoilMoisture

        try {
            task = service.getById(id)
            return ResponseEntity(task, HttpStatus.CREATED)
        } catch (e: Exception) {
            return ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    override fun save(obj: SoilMoisture): ResponseEntity<SoilMoisture> {
        val task: SoilMoisture

        try {
            task = service.create(obj)
            return ResponseEntity(task, HttpStatus.CREATED)
        } catch (e: Exception) {
            return ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
}