package csf.v50.iotgardenapi

import csf.v50.iotgardenapi.controller.AirHumidityController
import csf.v50.iotgardenapi.controller.AirTemperatureController
import csf.v50.iotgardenapi.controller.DefaultController
import csf.v50.iotgardenapi.controller.SoilMoistureController
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean

@SpringBootApplication
class IotGardenApiApplication

fun main(args: Array<String>) {
    runApplication<IotGardenApiApplication>(*args)
}

@Bean
fun defaultController() = DefaultController()

@Bean
fun airHumidityController() = AirHumidityController()

@Bean
fun airTemperatureController() = AirTemperatureController()

@Bean
fun soilMoistureController() = SoilMoistureController()
