package csf.v50.iotgardenapi.services

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.repository.MongoRepository

interface ISensorService<ISensorData> {
    fun GetAllPaginated(pageable: Pageable): Page<ISensorData>
    fun getById(id: String): ISensorData
    fun create(obj: ISensorData): ISensorData
}