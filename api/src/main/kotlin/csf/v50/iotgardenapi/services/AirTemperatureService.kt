package csf.v50.iotgardenapi.services

import csf.v50.iotgardenapi.model.AirTemperature
import csf.v50.iotgardenapi.repository.AirTemperatureRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service("AirTemperatureService")
class AirTemperatureService :ISensorService<AirTemperature> {

    @Autowired
    private lateinit var repository: AirTemperatureRepository

    override fun GetAllPaginated(pageable: Pageable): Page<AirTemperature> {
        return repository.findAll(pageable)
    }

    override fun getById(id: String): AirTemperature {
        return repository.findById(id).get()
    }

    override fun create(obj: AirTemperature): AirTemperature {
        return repository.save(obj)
    }
}