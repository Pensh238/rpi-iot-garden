package csf.v50.iotgardenapi.repository

import csf.v50.iotgardenapi.model.AirHumidity
import org.springframework.data.mongodb.repository.MongoRepository

interface AirHumidityRepository: MongoRepository<AirHumidity, String>