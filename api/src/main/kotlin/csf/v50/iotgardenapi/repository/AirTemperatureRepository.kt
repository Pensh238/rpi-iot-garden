package csf.v50.iotgardenapi.repository

import csf.v50.iotgardenapi.model.AirTemperature
import org.springframework.data.mongodb.repository.MongoRepository

interface AirTemperatureRepository: MongoRepository<AirTemperature, String>